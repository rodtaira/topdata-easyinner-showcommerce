﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EasyInnerSDK;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp;
using EasyInnerSDK.DAO;
using Newtonsoft.Json;
using Flurl.Http;

namespace EasyInnerSDK.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]


        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());

        }
    }
}